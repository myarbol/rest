# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.
Pouvoir mettre fin à un projet

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Rechercher un projet public
Barre de progression
Simulation de paiement

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Rien, la cohésion du groupe est bonne

### Qu'avons nous observé ?
//

### Quelle décision prenons nous suite à cette expérience ? 
//

### Qu'allons nous tester durant les 2 prochaines heures ? 
Rien, la cohésion du groupe est bonne

### À quoi verra-t-on que celà à fonctionné ?
//

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.