# Rétrospective de sprint

Nom du scrum master du sprint : Ferron Anaïs

## Ce que nous avons fait durant ce sprint
On a fait marcher le formulaire d'inscription sur le site.
Début de l'appli mobile

## Ce que nous allons faire durant le prochain sprint
Connexion depuis le site et l'appli.

## PDCA 
### Qu'avons nous testé durant ce sprint ?
Android Studio

### Qu'avons nous observé ?
Fonctionne à nouveau, début de l'appli.

### Quelle décision prenons nous suite à cette expérience ?


### Qu'allons nous tester durant les 2 prochaines heures ? 
Switcher les équipes afin que tout le monde travaille sur chaque tâche.

### À quoi verra-t-on que celà à fonctionné ?
Si tout le monde a eu des connaissances plus élargies sur le projet.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.
