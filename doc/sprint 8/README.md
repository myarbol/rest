# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.
Formulaire pour créer un projet créé
Création de la table te de la liste de transactions

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Finir la connection à un compte sur l'application mobile
Connecter le formulaire et la création de projet sur la BDD
Evaluer le solde actuel d'un projet
Créer la simulation de paiement

## PDCA 
### Qu'avons nous testé durant ce sprint ?
Changer la personne chargée de la partie graphique de l'application

### Qu'avons nous observé ?
Le développement de cet aspect s'est accéléré

### Quelle décision prenons nous suite à cette expérience ?
Laisser les rôles tels quels

### Qu'allons nous tester durant les 2 prochaines heures ?
Rien, la cohésion de groupe est bonne

### À quoi verra-t-on que celà à fonctionné ?
//

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.