# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément

## Ce que nous avons fait durant ce sprint
-> Connexion entre java et la base de données

## Ce que nous allons faire durant le prochain sprint
-> Connexion android à la base de données
-> Se connecter sur le site

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
-> Switcher les équipes afin que tout le monde travaille sur chaque tâche.

### Qu'avons nous observé ? 
-> Nous avons perdu trop de temps à tout epliquer pour que le binôme puisse travailler ensemble.

### Quelle décision prenons nous suite à cette expérience ?
-> Ne plus appliquer cette idée

### Qu'allons nous tester durant les 2 prochaines heures ? 
-> Retrouver notre stabilité.

### À quoi verra-t-on que celà à fonctionné ?
-> Si tout le monde peut a nouveau travailler efficacement.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.