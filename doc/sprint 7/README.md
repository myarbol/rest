# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint

Lister les différents projets
Connecter l'application mobile au serveur REST

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Pouvoir se connecter au serveur sur l'application mobile
Créer un formulaire pour créer un projet

## PDCA 
### Qu'avons nous testé durant ce sprint ?
Communiquer plus avec l'équipe de l'application mobile

### Qu'avons nous observé ?
Nous pouvons les aider à régler quelques problèmes plus facilement

### Quelle décision prenons nous suite à cette expérience ?
Continuer à bien communiquer avec eux

### Qu'allons nous tester durant les 2 prochaines heures ?
Accélérer l'avancement de la partie graphique de l'application en changeant de rôle une personne

### À quoi verra-t-on que cela à fonctionné ?
La partie graphique de l'application aura plus avancé

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.