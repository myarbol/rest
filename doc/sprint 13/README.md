# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.
Simuler un paiement
Barre de progession financement projet
Afficher la description d'un projet

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Afficher des infos du projet (mobile)

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Garder le changement de place

### Qu'avons nous observé ? 
Aucun Problème

### Quelle décision prenons nous suite à cette expérience ? 
La personne absente étant revenue, nous reprenons nos places habituelles

### Qu'allons nous tester durant les 2 prochaines heures ?
//

### À quoi verra-t-on que celà à fonctionné ?
//

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.