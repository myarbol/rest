# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

Rechercher un projet public

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Simulation de paiement
Afficher les détails d'un projet (mobile et web)

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Changer le role d'une personne suite à une absence

### Qu'avons nous observé ?
Il a réussi à reprendre le code de la personne absente

### Quelle décision prenons nous suite à cette expérience ? 
Le laisser à cette place juysqu'au retour de la personne absente

### Qu'allons nous tester durant les 2 prochaines heures ?
//

### À quoi verra-t-on que celà à fonctionné ?
//

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.