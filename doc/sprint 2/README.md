# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément 

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

-> Nous avons avancés sur les formulaire d'inscription et de connexion mais ils ne sont pas finis.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

-> Finir les formulaire d'inscription et de connexion.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
-> Nous avons testé d'avoir une meilleur communication

### Qu'avons nous observé ?

-> Cette communication nous a permis de rester bien à jour sur ce que fait tel ou tel personne et nous a permis d'avancé plus efficacement.

### Quelle décision prenons nous suite à cette expérience ? 

-> De continuer cette communication

### Qu'allons nous tester durant les 2 prochaines heures ? 

-> En apprendre plus sur les manipulation de sécurité des login mdp sur un single page javascript,  ainsi que sur SQLite fournis dans le serveur.

### À quoi verra-t-on que celà à fonctionné ?

-> Si nos requête SQLite ont fonctionné. 

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.