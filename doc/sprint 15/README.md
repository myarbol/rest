# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

-> Amélioration Viseul des projet
-> Somme restante manquante pour le projet
-> Présentation du site
-> Partenaire du site

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

-> Ajouter Transaction sur android
-> Docker

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
-> Notre cohésion est bonne nous n'avons rien testé

### Qu'avons nous observé ? 

### Quelle décision prenons nous suite à cette expérience ? 

### Qu'allons nous tester durant les 2 prochaines heures ? 
-> Rien

### À quoi verra-t-on que celà à fonctionné ?

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.