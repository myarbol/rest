# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

-> Connexion sur l'application mobile.
-> Connexion sur site internet.

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

-> Lister les projets

## PDCA 
### Qu'avons nous testé durant ce sprint ? 

-> Nous somme revenue a l'organisation de départ

### Qu'avons nous observé ?

-> Nous avons regagné notre stabilité

### Quelle décision prenons nous suite à cette expérience ? 

-> Ne plus changer les équipes sans vrai raison

### Qu'allons nous tester durant les 2 prochaines heures ? 

-> Plus communiquer avec le groupe qui fait l'application mobile

### À quoi verra-t-on que celà à fonctionné ?

-> L'application mobile sera à jours par rapport aux fonctionnalité du serveur.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.