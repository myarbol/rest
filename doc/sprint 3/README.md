# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

-> Aucune (mais on a enfin reussi à faire le lien entre le serveur et la base de donnée) 

## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

-> Finir le formulaire de Connexion et celui d'Inscription.

## PDCA 
### Qu'avons nous testé durant ce sprint ? 

-> De nous documentez sur SQLite

### Qu'avons nous observé ? 

-> On a reussi a faire marcher les connexion entre la base de donnée SQLite et le serveur

### Quelle décision prenons nous suite à cette expérience ? 

-> Continuer a mieux se documenter sur les différents outils

### Qu'allons nous tester durant les 2 prochaines heures ? 

-> Faire marcher Android Studio 

### À quoi verra-t-on que celà à fonctionné ?

-> Si nous avons pu commencer l'application mobile.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.