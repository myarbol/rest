# Rétrospective de sprint

Nom du scrum master du sprint : Derache Lucas

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

Connection à un compte sur l'appli mobile
Evaluer le solde actuel d'un projet
Evaluer le temps restant d'un projet
Créer la simulation de paiement
Formulaire pour créer un projet relié à la BDD


## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

Stats admin
Afficher la liste des projets sur l'application mobile
Pouvoir mettre fin à un projet


## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Rien, la cohésion de groupe est bonne

### Qu'avons nous observé ? 
//

### Quelle décision prenons nous suite à cette expérience ? 
//

### Qu'allons nous tester durant les 2 prochaines heures ? 
Rien, la cohésion de groupe est bonne

### À quoi verra-t-on que cela à fonctionné ?
//

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.