# Rétrospective de sprint

Nom du scrum master du sprint : LE RAY Clément

## Ce que nous avons fait durant ce sprint
Donnez ici la liste des histoires utilisateurs que vous avez livrés durant ce sprint.

-> Page d'acceuil


## Ce que nous allons faire durant le prochain sprint
Donnez ici la liste des histoires utilisateurs que vous vous engagez à terminer durant le prochain sprint.

-> Formulaire Inscription
-> Formulaire Connexion

## PDCA 
### Qu'avons nous testé durant ce sprint ? 

### Qu'avons nous observé ? 

### Quelle décision prenons nous suite à cette expérience ? 

### Qu'allons nous tester durant les 2 prochaines heures ? 

-> Avoir une meilleure communication

### À quoi verra-t-on que celà à fonctionné ?

-> Si nous avançont plus efficacemement.

# Mémo
N'oubliez pas d'ajouter une photo du radiateur d'information au moment de la rétrospective.