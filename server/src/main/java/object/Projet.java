package object;

import java.sql.Date;

public class Projet {
	
	private int pno;
	private String titre;
	private String description;
	private int somme;
	private String cheminImage;
	private String dateFinale;
	private String frequencePaiement;
	public boolean prive;
	private User user;
	private String mail;
	
	public Projet(int pno, String mail, String titre, String description, int somme, String cheminImage, String dateFinale, String frequencePaiement, boolean prive, User user){
		this.pno = pno;
		this.titre = titre;
		this.description = description;
		this.somme = somme;
		this.cheminImage = cheminImage;
		this.dateFinale = dateFinale;
		this.frequencePaiement = frequencePaiement;
		this.prive = prive;
		this.user = user;
		this.mail = mail;
	}
	
	public String getDateFinale() {
		return dateFinale;
	}

	public void setDateFinale(String dateFinale) {
		this.dateFinale = dateFinale;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Projet() {}
	
	
	
	public int getPno() {
		return pno;
	}
	
	public void setPno(int pno) {
		this.pno = pno;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getSomme() {
		return somme;
	}
	
	public void setSomme(int somme) {
		this.somme = somme;
	}
	
	public String getCheminImage() {
		return cheminImage;
	}
	
	public void setCheminImage(String cheminImage) {
		this.cheminImage = cheminImage;
	}
	
	
	
	public String getFrequencePaiement() {
		return frequencePaiement;
	}
	
	public void setFrequencePaiement(String frequencePaiement) {
		this.frequencePaiement = frequencePaiement;
	}
	
	public boolean isPrive() {
		return prive;
	}
	
	public void setPrive(boolean prive) {
		this.prive = prive;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	

}
