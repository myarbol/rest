package object;

import java.sql.Date;

import fr.iutinfo.skeleton.api.UserDTO;

public class User {
	
	private String role;
	private String nom;
	private String prenom;
	private String mail;
	private String mdp;
	private String ville;
	private String adresse;
	private String cheminimage;
	private String numerocarte;
	private String moyenpaiement;
	private String codeSecurite;
	
	public User(String role, String nom, String prenom, String mail, String mdp, String ville, String adresse, String cheminImage, String numeroCarte, String moyenPaiement, String codeSecurite) {
		this.role = role;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.mdp = mdp;
		this.ville = ville;
		this.adresse = adresse;
		this.cheminimage = cheminImage;
		this.numerocarte = numeroCarte;
		this.moyenpaiement = moyenPaiement;
		this.codeSecurite = codeSecurite;
	}
	
	public User(String nom, String prenom, String mail, String mdp) {
		this.nom=nom;
		this.prenom=prenom;
		this.mail=mail;
		this.mdp=mdp;
	}
	
	public User(){}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getCheminImage() {
		return cheminimage;
	}

	public void setCheminImage(String cheminImage) {
		this.cheminimage = cheminImage;
	}

	public String getNumeroCarte() {
		return numerocarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numerocarte = numeroCarte;
	}

	public String getMoyenPaiement() {
		return moyenpaiement;
	}

	public void setMoyenPaiement(String moyenPaiement) {
		this.moyenpaiement = moyenPaiement;
	}

	public String getCodeSecurite() {
		return codeSecurite;
	}

	public void setCodeSecurite(String codeSecurite) {
		this.codeSecurite = codeSecurite;
	}

	@Override
	public String toString() {
		return "User [role=" + role + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail
				+ ", mdp=" + mdp + ", ville=" + ville + ", adresse=" + adresse + " "
				+ ", cheminImage=" + cheminimage + ", numeroCarte=" + numerocarte + ", moyenPaiement=" + moyenpaiement
				+ ", codeSecurite=" + codeSecurite + "]";
	}
	
	
	
	public UserDTO toDto() {
		return new UserDTO(this);
	}
	

}
