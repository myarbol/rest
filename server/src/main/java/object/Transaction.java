package object;

import java.sql.Date;

public class Transaction {
	
	private int tno;
	private String date;
	private int somme;
	private String nom;
	private String prenom;
	private String mail;
	private String pno;
	
	
	public Transaction(int tno, String date, int somme, String nom, String prenom, String mail, String pno){
		this.tno = tno;
		this.date = date;
		this.somme = somme;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.pno = pno;
	}
	public Transaction() {}
	
	
	public int getTno() {
		return tno;
	}
	
	public void setTno(int tno) {
		this.tno = tno;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}

	public int getSomme() {
		return somme;
	}

	public void setSomme(int somme) {
		this.somme = somme;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPno() {
		return pno;
	}

	public void setPno(String pno) {
		this.pno = pno;
	}
	
	

}
