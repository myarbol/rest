package dao;


import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import object.User;

import java.util.List;

public interface UserDao {
   

	@SqlUpdate("CREATE TABLE users(" +  
			"role TEXT NOT NULL," + 
			"nom TEXT NOT NULL," + 
			"prenom TEXT NOT NULL," + 
			"mail TEXT NOT NULL PRIMARY KEY," + 
			"mdp TEXT NOT NULL," + 
			"ville TEXT," + 
			"adresse TEXT," + 
			"naissance DATE," + 
			"cheminimage TEXT," + 
			"numerocarte TEXT," + 
			"moyenpaiement TEXT," + 
			"codesecurite TEXT);" + 
			"")
	void createUserTable();
	

	
	@SqlUpdate("Drop table transacs; Drop table projet; Drop table users;")
	void dropTables();
	
	
	@SqlUpdate("Drop table users")
	void dropUserTable();
	
    @SqlQuery("select * from users")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<User> all();

    @SqlUpdate("insert into users (role, nom, prenom, mail, mdp) values ('user', :nom , :prenom , :mail , :mdp )")
    @RegisterMapperFactory(BeanMapperFactory.class)
    void insert(@Bind("nom") String nom,@Bind("prenom") String prenom, @Bind("mail") String mail, @Bind("mdp") String mdp );
    
    @SqlUpdate("insert into users (role, nom, prenom, mail, mdp) values ('admin', :nom , :prenom , :mail , :mdp )")
    @RegisterMapperFactory(BeanMapperFactory.class)
    void insertAdmin(@Bind("nom") String nom,@Bind("prenom") String prenom, @Bind("mail") String mail, @Bind("mdp") String mdp );
    
    @SqlQuery("select * from users where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findById(@Bind("id") int id);

    
    @SqlUpdate("delete from users")
    @RegisterMapperFactory(BeanMapperFactory.class)
    void delete();
    
    @SqlQuery("select * from users where mail = :mail AND mdp = :mdp")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User exists(@Bind("mail") String mail, @Bind("mdp") String mdp);
    
    @SqlUpdate("DROP TABLE PROJET")
    @RegisterMapperFactory(BeanMapperFactory.class)
    void deleteProjet();
    
   
    void close();

    
    @SqlQuery("select * from users where mail = :mail")
    @RegisterMapperFactory(BeanMapperFactory.class)
	User exists(@Bind("mail") String mail);
    
    
    @SqlQuery("select COUNT(*) from users")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int countUsers();
    
}