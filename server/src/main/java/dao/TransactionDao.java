package dao;


import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import object.Transaction;

import java.util.List;
public interface TransactionDao {
	
	
	@SqlUpdate("CREATE TABLE transacs(" + 
			"tno INTEGER PRIMARY KEY AUTOINCREMENT," + 
			"date DATE NOT NULL," + 
			"somme INTEGER NOT NULL," + 
			"nom TEXT NOT NULL," + 
			"prenom TEXT NOT NULL," + 
			"mail TEXT NOT NULL," + 
			"pno INTEGER," + 
			"FOREIGN KEY(pno) REFERENCES projet(pno));")
	void createTransactionTable();
	
	
	@SqlQuery("select * from transacs")
	@RegisterMapperFactory(BeanMapperFactory.class)
	List<Transaction> all();
	
	@SqlUpdate("drop table transacs")
	@RegisterMapperFactory(BeanMapperFactory.class)
	void drop();
	
	
	
	@SqlUpdate("Insert into transacs (date, somme, nom, prenom, mail, pno) values ( :date , :somme , :nom , :prenom , :mail, :pno )")
	@RegisterMapperFactory(BeanMapperFactory.class)
	void insert(@Bind("date") String date, @Bind("somme") String somme, @Bind("nom") String nom, @Bind("prenom") String prenom, @Bind("mail") String mail, @Bind("pno") String pno);

	@SqlQuery("Select SUM(somme) from transacs where pno = :pno ")
	@RegisterMapperFactory(BeanMapperFactory.class)
	int soldeActuel(@Bind("pno") String pno);
	
	@SqlQuery("Select SUM(somme) from transacs")
	@RegisterMapperFactory(BeanMapperFactory.class)
	int totalArgent();
}
