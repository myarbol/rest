package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Sample
{
  public static void main(String[] args) throws ClassNotFoundException
  {
    // load the sqlite-JDBC driver using the current class loader
    Class.forName("org.sqlite.JDBC");

    Connection connection = null;
    try
    {
      // create a database connection
      connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
      Statement stmt = connection.createStatement();
      /*stmt.executeUpdate("CREATE TABLE users(" + 
      		"uno SERIAL PRIMARY KEY," + 
      		"role TEXT NOT NULL," + 
      		"nom TEXT NOT NULL," + 
      		"prenom TEXT NOT NULL," + 
      		"mail TEXT NOT NULL," + 
      		"mdp TEXT NOT NULL," + 
      		"ville TEXT," + 
      		"adresse TEXT," + 
      		"naissance DATE," + 
      		"cheminImage TEXT," + 
      		"numeroCarte TEXT," + 
      		"moyenPaiement TEXT," + 
      		"codeSecurite TEXT);" + 
      		"");*/
      
      ResultSet rs = stmt.executeQuery("Select * from users");
      if(rs.next()) {
    	  System.out.println(rs.getString("prenom"));
      }
    }catch(Exception e) {
    	
    }
  }
}