package dao;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import object.*;

import java.util.List;

public interface ProjetDao {

	@SqlUpdate("CREATE TABLE projet(" + 
			"pno INTEGER PRIMARY KEY AUTOINCREMENT," + 
			"titre TEXT NOT NULL," + 
			"description TEXT," + 
			"somme INTEGER NOT NULL," + 
			"cheminImage TEXT," + 
			"dateFinale DATE NOT NULL," + 
			"frequencePaiement TEXT," + 
			"prive BOOLEAN NOT NULL," + 
			"mail TEXT," + 
			"FOREIGN KEY(mail) REFERENCES users(mail));")
	void createProjectTable();
	


	@SqlQuery("select * from projet")
	@RegisterMapperFactory(BeanMapperFactory.class)
	List<Projet> all();


	@SqlQuery("select * from projet where mail = :mail")
	@RegisterMapperFactory(BeanMapperFactory.class)
	List<Projet> findByMail(@Bind("mail") String mail);

	@SqlUpdate("delete from projet")
	@RegisterMapperFactory(BeanMapperFactory.class)
	void delete();

	
	@SqlUpdate("insert into projet (titre,mail,  somme, dateFinale, prive) values( :titre, :mail , :somme , :date , :prive ) ")
	@RegisterMapperFactory(BeanMapperFactory.class)
	void insertPro(@Bind("titre") String titre, @Bind("mail") String mail, @Bind("somme") String somme, @Bind("date") String date, @Bind("prive") String prive);
	
	
	void close();


	@SqlQuery("select * from projet where pno = :pno")
	@RegisterMapperFactory(BeanMapperFactory.class)
	List<Projet> get(@Bind("pno")String pno);
	
	
	@SqlQuery("select COUNT(*) from projet")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int countProjets();

	@SqlQuery("select COUNT(*) from projet where prive = 1")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int countProjetsPrives();
	
	@SqlUpdate("delete from projet where pno = :pno")
    @RegisterMapperFactory(BeanMapperFactory.class)
	int deleteProjet(@Bind("pno") String pno);


	@SqlQuery("Select * from projet where prive = 0")
    @RegisterMapperFactory(BeanMapperFactory.class)
	List<Projet> allByName();

	
	@SqlUpdate("Update projet set description = :description where pno = :pno")
    @RegisterMapperFactory(BeanMapperFactory.class)
	void changeDescription(@Bind("description") String description, @Bind("pno") String pno);
}


