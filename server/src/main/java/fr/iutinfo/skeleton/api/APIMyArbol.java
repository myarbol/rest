package fr.iutinfo.skeleton.api;

import javax.ws.rs.*;
import java.util.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.ws.rs.core.*;


import dao.ProjetDao;
import dao.TransactionDao;
import dao.UserDao;
import object.Projet;
import object.Transaction;
import object.User;

import java.util.*;

import java.lang.reflect.Field;
import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("")
public class APIMyArbol {
	private static UserDao daoUser = getDbi().open(UserDao.class);
	private static ProjetDao daoProjet = getDbi().open(ProjetDao.class);
	private static TransactionDao daoTransac = getDbi().open(TransactionDao.class);

	public APIMyArbol() throws SQLException {
		// TODO Auto-generated constructor stub

		if (!tableExist("users")) {
			daoUser.createUserTable();
		}
		if (!tableExist("projet")) {
			daoProjet.createProjectTable();
		}
		if (!tableExist("transacs")) {
			daoTransac.createTransactionTable();
		}
	}

	
	
	@GET
	@Path("/delete/delete")
	public void delete() {
		daoUser.dropTables();
	}
	/**
	 * USERS
	 */
	
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users")
	public List<User> getUsers() {
		List<User> users = daoUser.all();
		return users;
	}

	

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/verif/")
	public User getUser(@FormParam("mail") String mail, @FormParam("mdp") String mdp) {
		System.out.println("mail : " + mail + " mdp " + mdp);
		User u = daoUser.exists(mail, mdp);
		if (u == null) {
			throw new NotFoundException();
		} else {
			return u;
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/verif/{mail}")
	public User getUser(@PathParam("mail") String mail) {
		User u = daoUser.exists(mail);
		if (u == null) {
			throw new NotFoundException();
		} else {
			return u;
		}
	}

	

	@GET
	@Path("/delete")
	public void deleteUser() {
		daoUser.delete();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/users/")
	public void createUser(@FormParam("nom") String nom, @FormParam("prenom") String prenom,
			@FormParam("mail") String mail, @FormParam("mdp") String mdp) {
		daoUser.insert(nom, prenom, mail, mdp);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/users/admin/")
	public void createUserAdmin(@FormParam("nom") String nom, @FormParam("prenom") String prenom,
			@FormParam("mail") String mail, @FormParam("mdp") String mdp) {
		daoUser.insertAdmin(nom, prenom, mail, mdp);
	}

	/**
	 * PROJETS
	 */
	
	
	
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/projets/")
	public void createProject(@FormParam("titre") String titre, @FormParam("mail") String mail,
			@FormParam("prive") String prive, @FormParam("dateFinale") String dateFinale,
			@FormParam("somme") String somme) {

		daoProjet.insertPro(titre, mail, somme, dateFinale, prive);
	}

	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Path("/projets/delete")
	public void deleteProjet() {
		daoUser.deleteProjet();
	}
	
	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/projet/{mail}")
	public List<Projet> getProjetByMail(@PathParam("mail") String mail) {
		List<Projet> p = daoProjet.findByMail(mail);
		return p;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/projets")
	public List<Projet> getProjets() {
		List<Projet> users = daoProjet.all();
		return users;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/projets/public/")
	public List<Projet> getProjetsPublics() {
		List<Projet> projets = daoProjet.allByName();
		return projets;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/projets/pno/{pno}")
	public List<Projet> getProjets(@PathParam("pno")String pno) {
		
		return daoProjet.get(pno);
	}
	
	
	@DELETE
	@Path("/projets/delete/{pno}")
	@Consumes("application/x-www-form-urlencoded")
	public void deleteProjet(@PathParam("pno") String pno) {
		daoProjet.deleteProjet(pno);
	}
	
	/**
	 * TRANSACTIONS
	 */
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/transactions")
	public List<Transaction>getTransactions(){
		List<Transaction> transac = daoTransac.all();
		return transac;
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/transactions/")
	public void createTransaction(@FormParam("date") String date, @FormParam("somme") String somme, @FormParam("nom") String nom, @FormParam("prenom") String prenom, @FormParam("mail") String mail, @FormParam("pno") String pno) {
		daoTransac.insert(date, somme, nom, prenom, mail, pno);
	}
	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/transactions/{pno}")
	public int getSoldeActuel(@PathParam("pno") String pno) {
		return daoTransac.soldeActuel(pno);
	}
	
	
	/**
	 * STATISTIQUES
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/stats/users")
	public int getCountUser() {
		return daoUser.countUsers();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/stats/projets")
	public int getCountProjet() {
		return daoProjet.countProjets();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/stats/projetsPrives")
	public int getCountProjetPrives() {
		return daoProjet.countProjetsPrives();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/stats/money")
	public int getCountTotal() {
		return daoTransac.totalArgent();
	}
	
	@GET
	@Path("/transactions/delete")
	public void deleteTransacs() {
		daoTransac.drop();
	}
	
	@PUT
	@Path("/projets/desc")
	public void changeDescr(@FormParam("description") String description,@FormParam("pno") String pno ) {
		daoProjet.changeDescription(description, pno);
	}
	
}
