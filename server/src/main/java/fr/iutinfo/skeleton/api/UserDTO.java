package fr.iutinfo.skeleton.api;

import object.User;

public class UserDTO {
	String nom ;
	String prenom;
	String uno;
	public UserDTO(User u) {
		this.nom = u.getNom();
		this.prenom = u.getPrenom();
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
}
