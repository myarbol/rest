var cmail="";
var role="user";
var tab = new Array();
$(document).ready(function() {
	$("#connect-user").show();
	check_form_connect();
	check_form_create();
	check_form_createproject();
	project_pub();
	$("#create-project").hide();
	$("#btnInscrire").click(function(event) {
		$("#create-user").show();
		$("#connect-user").hide();
	});
	$("#btnConnecter").click(function(event) {
		$("#connect-user").show();
		$("#create-user").hide();
	});
	
});

// AJAX REQUEST
function check_form_create() {
	$("#create-user").submit(function(event) {
		event.preventDefault();
		var error = false;
		if (!$("#i-nom").val()) {
			//show_error_nom();
			error = true;
		}
		if (!$("#i-prenom").val()) {
			//show_error_prenom();
			error = true;
		}
		if (!$("#i-mail").val()) {
			//show_error_mail();
			error = true;
		}
		if (!($("#i-mdp").val() === $("#i-mdp-2").val())) {
			//show_error_mdp();
			error = true;
		}
		if(!$("#i-mdp").val()) {
			//show_error_mdp_null();
			error = true;
		}
		if (!error) {
			create_user();
		} else {
			alert("vous avez mal complété le formulaire");
		}
		//$(this).reset();
	});
}

function check_form_connect() {
	$("#connect-user").submit(function(event) {
		event.preventDefault();
		var error = false;
		if (!$("#c-mail").val() && !$("#c-mdp").val()) {
			error = true;
		}
		if (!error) {
			connect_user();
			if(role=="admin"){
				project_list();
			}
			else{
			project_user();
			}
		} else {
			alert("Erreur de login");
		}
	});
}

function check_form_createproject() {
	$("#create-project").submit(function(event) {
		event.preventDefault();
		create_project();
	});
}


function create_user() {
	$.ajax({
		url:"http://localhost:8080/api/users/",
		type:"POST",

		data:{
			"nom":$("#i-nom").val(),
			"prenom":$("#i-prenom").val(),
			"mail":$("#i-mail").val(),
			"mdp":$("#i-mdp").val()
		},

		dataType:"JSON",
		success: function(json) {
			alert("Vous avez été inscrit");
		}
	});
}

function connect_user() {
		cmail=$("#c-mail").val();
	$.ajax({
		url:"http://localhost:8080/api/users/verif/",
		type:"POST",
		data:{
			"mail":$("#c-mail").val(),
			"mdp":$("#c-mdp").val()
		},

		dataType:"json",
		success: function(user, status){
			prenom=user.prenom;
               $("#session > h2").text("Bonjour "+user.prenom);
               console.log(user.role);
				if(user.role=='user'){
					project_user();
				}else{
					project_statsProjet();
				}
        },
		error: function(xhr, status, errorThrown){
			alert( "T ki toi?" );
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsProjet() {
	$.ajax({
		url:"http://localhost:8080/api/stats/projets",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			
			$("#content").prepend("<h4>Nombre de projet total :</h4><p>"+projets+"</p><br>");
			project_statsUsers();
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsUsers() {
	$.ajax({
		url:"http://localhost:8080/api/stats/users",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			
			$("#content").prepend("<h4>Nombre d'utilisateur total :</h4><p>"+projets+"</p>");
			project_statsMoney();
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsMoney() {
	$.ajax({
		url:"http://localhost:8080/api/stats/money",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			
			$("#content").prepend("<h4>Argent total investi :</h4><p>"+projets+"€</p>");
			
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_user() {

	$.ajax({
		url:"http://localhost:8080/api/projet/"+cmail,
		type:"GET",
		dataType:"json",
		success: function(projets, status){
		$("#topHomePage").hide();
		$("#presentation").hide();
		$("#create-project").show();
		$("#project").html("");
		for (var i = 0; i < projets.length; i++) {
			//var button = $("<button>Delete</button>").click(function(event){delete_project(projets[i].pno);});
			tab.push(projets[i].pno);
			var button = $("<button>Delete</button>").click(function(event){alert(tab[i]);});
            $("#project").append("<div class=\"project\"><h3>"+projets[i].titre+"</h3>");
            $("#project").append(button);
            $("#project").append( "<p class=\"sum\">Montant: "+projets[i].somme+"</p></div>");
    	}
        },
		
		error: function(xhr, status, errorThrown){
			alert( "Pas de projet" );
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
	
}

function create_project() {
	var priv=1;	
	if($('#p-private').prop('checked') == true){
		priv=0;
	}
	$.ajax({
		url:"http://localhost:8080/api/projets/",
		type:"POST",
		data:{
			"titre":$("#p-titre").val(),
			"somme":$("#p-somme").val(),
			"prive":priv,
			"dateFinale":$("#p-date").val(),
			"mail":cmail
		},
		dataType:"JSON",
		success: function(json) {
			alert("Vous avez créé votre projet");
		}
	});
	project_user();
}

function delete_project(project) {
	$.ajax({
		url:"http://localhost:8080/api/delete/"+project,
		type:"DELETE",
		success: function(json) {
			alert("Vous avez supprimé votre projet");
		}
	});
	project_user();
}

function project_pub() {
	$("#search-form").submit(function(event) {
		event.preventDefault();
		$.ajax({
			url:"http://localhost:8080/api/projets/",
			type:"GET",
			dataType:"json",
			success: function(projets, status){
				$("#topHomePage").hide();
				$("#presentation").hide();
				$("#project").html("");
				for (var i = 0; i < projets.length; i++) {
            	$("#project").append("<div class=\"project\"><h3>"+projets[i].titre+"</h3>"+"<p class=\"sum\">Montant: "+projets[i].somme+"</p></div>");
    			}
        	},
			error: function(xhr, status, errorThrown){
				alert("Pas de projet projet public à ce nom");
			},
		});
	});
}

