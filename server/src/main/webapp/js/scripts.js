var cmail="";
var role="user";
var nom="";
var prenom="";
var connected=false;

$(document).ready(function() {
	$("#connect-user").show();
	check_form_connect();
	check_form_create();
	check_form_createproject();
	project_pub();
	$("#create-project").hide();
	$("#btnInscrire").click(function(event) {
		event.preventDefault();
		$("#create-user").show();
		$("#connect-user").hide();
	});/*
	$(".homeRedirect").click(function(event) {
		event.preventDefault();
		$("#create-user").show();
		$("#connect-user").hide();
	});*/
	$("#btnConnecter").click(function(event) {
		event.preventDefault();
		$("#connect-user").show();
		$("#create-user").hide();
	});
	$("#lApropos").click(function(event) {
		event.preventDefault();
		$("#topHomePage").hide();
		$("#presentation").hide();
		$(".hide").hide();
		$("#session").hide();
		$("#a-propos").show();
	});
	$("#lPartenaire").click(function(event) {
		event.preventDefault();
		$("#topHomePage").hide();
		$("#presentation").hide();
		$(".hide").hide();
		$("#session").hide();
		$("#partenaire").show();
	});
});

// AJAX REQUEST
function check_form_create() {
	$("#create-user").submit(function(event) {
		event.preventDefault();
		var error = false;
		if (!$("#i-nom").val()) {
			//show_error_nom();
			error = true;
		}
		if (!$("#i-prenom").val()) {
			//show_error_prenom();
			error = true;
		}
		if (!$("#i-mail").val()) {
			//show_error_mail();
			error = true;
		}
		if (!($("#i-mdp").val() === $("#i-mdp-2").val())) {
			//show_error_mdp();
			error = true;
		}
		if(!$("#i-mdp").val()) {
			//show_error_mdp_null();
			error = true;
		}
		if (!error) {
			create_user();
		} else {
			alert("vous avez mal complété le formulaire");
		}
		//$(this).reset();
	});
}

function check_form_connect() {
	$("#connect-user").submit(function(event) {
		event.preventDefault();
		var error = false;
		if (!$("#c-mail").val() && !$("#c-mdp").val()) {
			error = true;
		}
		if (!error) {
			connect_user();
		} else {
			alert("Erreur de login");
		}
	});
}

function check_form_createproject() {
	$("#create-project").submit(function(event) {
		event.preventDefault();
		create_project();
	});
}


function create_user() {
	$.ajax({
		url:"/api/users/",
		type:"POST",

		data:{
			"nom":$("#i-nom").val(),
			"prenom":$("#i-prenom").val(),
			"mail":$("#i-mail").val(),
			"mdp":$("#i-mdp").val()
		},

		dataType:"JSON",
		success: function(json) {
			$("#create-user")[0].reset();
			alert("Vous avez été inscrit");
		}
	});
}

function connect_user() {
		cmail=$("#c-mail").val();
		connected=true;
	$.ajax({
		url:"/api/users/verif/",
		type:"POST",
		data:{
			"mail":$("#c-mail").val(),
			"mdp":$("#c-mdp").val()
		},

		dataType:"json",
		success: function(user, status){
			prenom=user.prenom;
			nom = user.nom;
               $("#session > h2").text("Bonjour "+user.prenom);
				if(user.role=='user'){
					project_user();
				}else{
					project_statsProjet();
				}
				
        },
		error: function(xhr, status, errorThrown){
			alert( "Login ou mdp inconnus" );
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsProjet() {
	$.ajax({
		url:"/api/stats/projets",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			
			$("#content").append("<h4>Nombre de projet total :</h4><p>"+projets+"</p><br>");
			project_statsUsers();
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsUsers() {
	$.ajax({
		url:"/api/stats/users",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			
			$("#content").append("<h4>Nombre d'utilisateur total :</h4><p>"+projets+"</p><br>");
			project_statsMoney();
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_statsMoney() {
	$.ajax({
		url:"/api/stats/money",
		type:"GET",
		dataType:"text",
		success: function(projets, status){
			console.log(projets);
			$("#topHomePage").hide();
			$("#presentation").hide();
			$("#create-project").show();
			$("#project").html("");
			$("#content").append("<h4>Argent total investi :</h4><p>"+projets+"€</p>");
			
        },
		error: function(xhr, status, errorThrown){
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function project_user() {
	$("#presentation").hide();
	$("#create-project").show();
	$("#searchbar").prependTo("header");
	$("#project").html("");
	$("#searchbar").prependTo("#project");
	$("#topHomePage").hide();
	$.ajax({
		url:"/api/projet/"+cmail,
		type:"GET",
		dataType:"json",
		success: function(projets, status){
		for (var i = 0; i < projets.length; i++) {
			var button = $("<button id ="+projets[i].pno+" class=\"supprimer\">Supprimer</button>").click(function(event){delete_project($(this).attr("id"));});
			var button2 = $("<button id ="+projets[i].pno+" class=\"modifier\">Modifier</button>").click(function(event){modify_project($(this).attr("id"));});
			
			if(projets[i].prive){
					$("#project").append("<div class=\"project\"><div><h3>"+projets[i].titre+" (privé)"+"</h3>");
			}else{
					$("#project").append("<div class=\"project\"><div><h3>"+projets[i].titre+" (public)"+"</h3>");
			}
			getTpsRestant(projets[i].pno,".projet");
			afficherBarre(projets[i].pno, projets[i].somme);
            $(".project").last().append(button);
            $(".project").last().append( "<p class=\"sum\">Montant demandé: "+projets[i].somme+"</p>");
            formulaireTransaction(projets[i].pno);
            formulaireModification(projets[i].pno, projets[i].description);
            //$(".project").last().append('</div><textarea class="details">'+projets[i].description+'</textarea></div>');
			
    	}
        },
		
		error: function(xhr, status, errorThrown){
			alert( "Pas de projet" );
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
		});
}

function getTpsRestant(pno,element){
	let now = new Date(Date.now());
	let date= null;
	$.ajax({
		url: "/api/projets/pno/"+pno,
		dataType: "json",
		async:false,
		success :function(data){
			
			date = data[0].dateFinale;
			var oui = date.substring(3,5) + "/" + date.substring(0,2) + date.substring(5);
			date = new Date(oui);

			var utcThis = Date.UTC(now.getYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds());
  			var utcOther = Date.UTC(date.getYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
			var dateFin = ( utcOther - utcThis) / 86400000;
			var res = Math.floor((dateFin/30))+" mois "+Math.floor((dateFin%30))+" jours";
			$(".project").last().append("<h4> "+res+" restants </h4>");
		},
		error : function(one, two, three){
			alert("Erreur");
		}
	});

}

function afficherBarre(pno, somme){	
	$.ajax({
		url:"/api/transactions/"+pno,
		type:"GET",
		dataType:"text",
		async:false,
		success: function(projets, status){
			var barre = $("<progress width=500 value = "+projets+" max = "+somme+"></progress> <p>Montant actuel :"+projets+" </p>")
			$(".project").last().append(barre);
        },
		
		error: function(xhr, status, errorThrown){
			alert( "Pas de projet" );
	        console.log( "Error: " + errorThrown );
	        console.log( "Status: " + status );
	        console.dir( xhr );
		}
	});
}

function formulaireTransaction(pno){
	var form="";
	if (connected) {
		form = $('<form><input id='+pno+' type=number placeholder=Montant><button id='+pno+' class=\"ajouter\">Ajouter Argent !</button></form>');
	} else {
		form = $('<form><input name=nom type=text placeholder=Nom><input name=prenom type=text placeholder=Prénom>'+
			'<input name=\"mail\" type=text placeholder=\"E-mail\">'+
			'<input name=\"somme\" id='+pno+' type=number placeholder=Montant><button id='+pno+' class=\"ajouter\">Ajouter Argent !</button></form>');
	}
	let button = form.children().last();
	let oui = form.children().first();
	button.css({"background-color" : "green"});
	button.click(function (event){
		event.preventDefault();
		if (connected) {
			creditproject(pno,oui.val());
		} else {
			nom = form.children()[0].value;
			prenom = form.children()[1].value;
			cmail = form.children()[2].value;
			creditproject(pno,form.children()[3].value);
		}
		//project_pub();
	});
	$(".project").last().append(form);
}

function formulaireModification(pno, description){
	var form = $('<form><textarea>'+description+'</textarea><button>Modifier</button></form>');
	let button = form.children().last();
	let oui = form.children().first();
	button.css({"background-color" : "blue"});
	button.click(function (event){
		event.preventDefault();
		console.log("coucou");
		//creditproject(pno,oui.val());
		modify_project(pno, oui.val());
	});
	$(".project").last().append(form);
}


function creditproject(pno, montant) {
	
		var date = new Date();
		if(montant>0){
		$.ajax({
			url:"/api/transactions",
			type:"POST",
			data:{
				"date":date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear(),
				"somme":montant,
				"nom":nom,
				"prenom":prenom,
				"mail":cmail,
				"pno":pno
			},

			dataType:"json",

			success: function(data) {
				alert("le projet a été crédité");
				if (connected) {
					project_user();
				} else {
					var forms = $("form");
					for(var i = 0; i<forms.length; i++){
						forms[i].reset();
					}
					oui();
				}
			},
			error: function(xhr, status, errorThrown){
				alert( "Pas possible de payer" );
	        	console.log( "Error: " + errorThrown );
	        	console.log( "Status: " + status );
	        	console.dir( xhr );
			}
		});
		}else{
			alert("Montant Incorrect");
}

}

function create_project() {
	var priv=1;	
	if($('#p-private').prop('checked') == true){
		priv=0;
	}
	$.ajax({
		url:"/api/projets/",
		type:"POST",
		data:{
			"titre":$("#p-titre").val(),
			"somme":$("#p-somme").val(),
			"prive":priv,
			"dateFinale":$("#p-date").val(),
			"mail":cmail
		},
		dataType:"JSON",
		success: function(json) {
			alert("Vous avez créé votre projet");
			$("#create-project")[0].reset();
			project_user();
		}
	});
	
}

function modify_project(pno, description) {
	$.ajax({
		url:"/api/projets/desc",
		type:"PUT",
		data:{
			"pno":pno,
			"description":description
		},
		dataType:"JSON",
		success: function(json) {
			alert("Vous avez modifié votre projet");
			project_user();
		}
	});
	
}

function delete_project(project) {
	$.ajax({
		url:"/api/projets/delete/"+project,
		type:"DELETE",
		success: function(json) {
			alert("Vous avez supprimé votre projet");
			project_user();
		}
	});
	
}

function project_pub() {
	$("#search-form").submit(function(event) {
		event.preventDefault();
		
		oui();
	});
	
}

function oui(){
	$("#presentation").hide();
		$("#searchbar").prependTo("header");
		$("#project").html("");
		$("#searchbar").prependTo("#project");
		$("#topHomePage").hide();
	$.ajax({
			url:"/api/projets/public",
			type:"GET",
			dataType:"json",
			
			async:false,
			success: function(projets, status){
				for (var i = 0; i < projets.length; i++) {
					var comp = projets[i].titre;
					if(comp.toLowerCase().includes($("#search").val().toLowerCase())){
         		  		 $("#project").append("<div class=\"project\"><h3>"+projets[i].titre+" ("+projets[i].mail+")</h3>");
           				 afficherBarre(projets[i].pno, projets[i].somme);
           				 $(".project").last().append( "<p class=\"sum\">Montant: "+projets[i].somme+"</p></div>");
           				 formulaireTransaction(projets[i].pno);
					}
    			}

    			
       		 },
			error: function(xhr, status, errorThrown){
				alert( "Pas de projet a ce nom" );
	        	console.log( "Error: " + errorThrown );
	        	console.log( "Status: " + status );
	        	console.dir( xhr );
			}
		});
}